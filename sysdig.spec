%define sysdigversion       0.28.0
%define libsversion         033c4b9f28e58e20a5822bd8a7419beea098af91
%define b64version          1.4.1
%define valijsonversion     0.6
%define kernelversion       %(uname -r |sed 's/-/_/g').1

Name:       sysdig
Version:    %{sysdigversion}
Release:    %{kernelversion}.1
Summary:    Linux system exploration and troubleshooting tool with first class support for containers

License:    Apache v2.0
URL:        https://github.com/draios/sysdig
Source0:    https://github.com/draios/sysdig/archive/refs/tags/%{sysdigversion}.tar.gz#/%{name}-%{sysdigversion}.tar.gz
Source1:    https://github.com/falcosecurity/libs/archive/%{libsversion}.tar.gz
Source2:    https://github.com/libb64/libb64/archive/v%{b64version}.tar.gz#/libb64-%{b64version}.tar.gz
Source3:    https://github.com/tristanpenman/valijson/archive/refs/tags/v%{valijsonversion}.tar.gz#/valijson-%{valijsonversion}.tar.gz
Source4:    0001-openeuler.patch

#custom patch

BuildRequires:  kernel-devel git patch elfutils-libelf-devel
BuildRequires:  make cmake gcc gcc-c++ tbb-devel jq-devel libcurl-devel jsoncpp-devel
BuildRequires:  c-ares-devel luajit-devel grpc-devel grpc-plugins protobuf-devel ncurses-devel
#for bpf
BuildRequires:  clang llvm
#for obs
%if 0%{?distribution:1}
BuildRequires:  uname-build-checks
%endif

Requires:   abseil-cpp brotli c-ares cyrus-sasl-lib e2fsprogs elfutils-libelf glibc grpc
Requires:   jq jsoncpp keyutils-libs krb5-libs libcurl libgcc libidn2 libnghttp2 libpsl libselinux libssh
Requires:   libstdc++ libunistring libxcrypt luajit openldap openssl-libs pcre2 protobuf re2 tbb zlib

%description
Sysdig is a universal system visibility tool with native support for containers:
~$ sysdig
Csysdig is a simple, intuitive, and fully customizable curses UI for sysdig:
~$ csysdig

%prep
tar xf %{SOURCE1}
patch -d libs-%{libsversion}/ -p1 < %{SOURCE4}
tar xf %{SOURCE2}
tar xf %{SOURCE3}
%autosetup -n %{name}-%{sysdigversion} -p1 -Sgit

%build
export CFLAGS="-fPIC -Wl,--copy-dt-needed-entries $RPM_OPT_FLAGS"
export CXXFLAGS="-fPIC -Wl,--copy-dt-needed-entries $RPM_OPT_FLAGS"
export LDFLAGS="-fPIC -Wl,--copy-dt-needed-entries $RPM_LD_FLAGS"
cd ../libb64-%{b64version}
%make_build all_base64
cd ../valijson-%{valijsonversion}
mkdir build
cd build
cmake ..
%make_build
cd ../../%{name}-%{sysdigversion}
mkdir build
cd build
cmake -DFALCOSECURITY_LIBS_SOURCE_DIR=$(readlink -f ../../libs-%{libsversion}/) \
        -DUSE_BUNDLED_DEPS=false \
        -DVALIJSON_INCLUDE=$(readlink -f ../../valijson-%{valijsonversion}/include/) \
        -DB64_INCLUDE=$(readlink -f ../../libb64-%{b64version}/include/) \
        -DB64_LIB=$(readlink -f ../../libb64-%{b64version}/src/libb64.a) \
        -DCMAKE_INSTALL_PREFIX=%{_prefix} \
        -DBUILD_BPF=ON \
    ..
%make_build

%install
cd build
export INSTALL_MOD_PATH=%{buildroot}
%make_install PREFIX=%{_prefix} install_driver
mkdir -p %{buildroot}/lib/modules/%(uname -r)/extra/bpf
install -m 640 driver/bpf/probe.o %{buildroot}/lib/modules/%(uname -r)/extra/bpf/scap-bpf.o

%post
/sbin/depmod -a
mkdir -p ~/.scap/
ln -sf /lib/modules/%(uname -r)/extra/bpf/scap-bpf.o ~/.scap/scap-bpf.o

%postun
/sbin/depmod -a

%files
%defattr(-,root,root)
%doc COPYING README.md
%{_bindir}/*
%{_prefix}/src/scap-*/*
%{_datarootdir}/%{name}/*
%{_datarootdir}/zsh/*
%{_prefix}/etc/*
%{_mandir}/man8/*.8.gz
/lib/modules/*/extra/*
%exclude /lib/modules/*/modules.*

%changelog
* Mon Feb 7 2022 Zhipeng Xie<xiezhipeng1@huawei.com> - 0.28.0-1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:package init
