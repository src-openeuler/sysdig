rm -rf *.tar.gz
commit=$(cat sysdig.spec |grep "define libsversion"|awk '{print $NF}')
curl -L -O https://github.com/falcosecurity/libs/archive/${commit}.tar.gz
version=$(cat sysdig.spec |grep "define sysdigversion" |awk '{print $NF}')
curl -L -O https://github.com/draios/sysdig/archive/refs/tags/${version}.tar.gz#/sysdig-${version}.tar.gz
version=$(cat sysdig.spec |grep "define b64version"|awk '{print $NF}')
curl -L -O https://github.com/libb64/libb64/archive/v${version}.tar.gz#/libb64-${version}.tar.gz
version=$(cat sysdig.spec |grep "define valijsonversion"|awk '{print $NF}')
curl -L -O https://github.com/tristanpenman/valijson/archive/refs/tags/v${version}.tar.gz#/valijson-${version}.tar.gz
